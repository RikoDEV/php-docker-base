ARG PHP_VERSION
FROM php:${PHP_VERSION}-fpm-alpine

RUN apk add --no-cache --update nginx dcron unzip git pcre-dev ${PHPIZE_DEPS} bzip2-dev icu-dev libpng-dev freetype-dev libjpeg-turbo-dev libwebp-dev libavif-dev libxml2-dev libzip-dev \
    && pecl install redis \
    && docker-php-ext-enable redis \
    && docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp --with-avif \
    && docker-php-ext-install bz2 pdo_mysql xml zip exif intl gd opcache \
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
    && rm -rf /tmp/pear /usr/src/