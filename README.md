# PHP Docker Base
Basic PHP docker image created for my personal Laravel projects.

## Uses:
* PHP 8.2 [redis, opcache, gd, zip, xml, exif, composer]
* PHP 8.3 [redis, opcache, gd, zip, xml, exif, composer]
* PHP 8.4 [redis, opcache, gd, zip, xml, exif, composer]